# VRDL HW4 image segmentation

Follow the steps below to reproduce my result

## Place the dataset
1. The dataset can be find at: https://github.com/NCTU-VRDL/CS_IOC5008/tree/master/HW4
2. Set your dataset as:
    ```
    - FOLDER
        - train_images
            - all the train images
        - test_images
            - all the test images
        - pascal_train.json
        - test.json
    ```
## Run prediction with trained weight
1. `cd keras-maskrcnn/keras_maskrcnn/bin`
2. Open `evaluate.py`
3. Modify the weight name in these lines: 
    ```python=142
    print('Loading model, this may take a second...')
    model_name = 'resnet50_coco_10'  #### weight file name ####
    model_path = os.path.join('..', '..', 'snapshots', model_name+'.h5')
    ```
3. Save and run `python evaluate.py coco /PATH/TO/DATASET/FOLDER`
4. The submit file will be saved in `keras-maskrcnn/keras_maskrcnn/bin/sub_WEIGHT-FILE-NAME.json`

**Best weight is resnet50_coco_09.h5**

## Run train process
1. `cd keras-maskrcnn/keras_maskrcnn/bin`
2. Open `train.py`
3. Modify the output weight name in these lines:
    ```python=83
    # save the prediction model
    if args.snapshots:
        # ensure directory created first; otherwise h5py will error after epoch.
        os.makedirs(args.snapshot_path, exist_ok=True)
        checkpoint = keras.callbacks.ModelCheckpoint(
            os.path.join(
                args.snapshot_path,
                '{backbone}_{dataset_type}_{{epoch:02d}}.h5'.format(backbone=args.backbone, dataset_type=args.dataset_type) #### The saved weight file name ####
            ),
            verbose=1
        )
        checkpoint = RedirectModel(checkpoint, model)
        callbacks.append(checkpoint)
    ```
3. Set the data augmentations in transform_generator in function create_generators: 
    ```python=139
    # create random transform generator for augmenting training data
    transform_generator = random_transform_generator(flip_x_chance=0.5)
    #transform_generator = random_transform_generator(
    #                    min_rotation=-0.1,
    #                    max_rotation=0.1,
    #                    min_translation=(-0.1, -0.1),
    #                    max_translation=(0.1, 0.1),
    #                    min_shear=-0.1,
    #                    max_shear=0.1,
    #                    min_scaling=(0.9, 0.9),
    #                    max_scaling=(1.1, 1.1),
    #                    flip_x_chance=0.5,
    #                    flip_y_chance=0.5
    #)
    ```
    *I only use the `flip_x_chance=0.5` in my submission due to the SUPER LONG training time, I don't have time to try all of them.*
5. Save and run `python train.py coco /PATH/TO/DATASET/FOLDER`

**The training process takes about 5 days to get to epoch 10 using my settings**

## Reference
Code from https://github.com/fizyr/keras-maskrcnn